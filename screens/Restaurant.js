import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, SafeAreaView} from 'react-native';
import HomeHeader from '../components/HomeHeader';
import RestaurantHeader from '../components/RestaurantHeader';
import {COLORS, icons} from '../constants';

const Restaurant = ({route, navigation}) => {
  const [currentLocation, setCurrentLocation] = useState({});
  const [restaurant, setRestaurant] = useState({});

  useEffect(() => {
    const {currentLocation, item} = route.params;
    console.log(currentLocation);
    setCurrentLocation(currentLocation);
    setRestaurant(item);
  }, [route.params]);

  return (
    <SafeAreaView style={styles.container}>
      <RestaurantHeader restaurant={restaurant} navigation={navigation} />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.lightGray2,
  },
});

export default Restaurant;
