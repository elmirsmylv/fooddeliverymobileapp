import React, {useState} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
  Image,
  StyleSheet,
} from 'react-native';

import {categoryData, restaurantData, initialCurrentLocation} from '../data';
import HomeHeader from '../components/HomeHeader';
import {COLORS, FONTS, icons, images, SIZES} from '../constants';
import MainCategories from '../components/MainCategories';
import RestaurantList from '../components/RestaurantList';

const Home = ({navigation}) => {
  const [categories, setCategories] = useState(categoryData);
  const [selectedCategory, setSelectedCategory] = useState(null);
  const [restaurants, setRestaurants] = useState(restaurantData);
  const [currentLocation, setCurrentLocation] = useState(
    initialCurrentLocation,
  );

  const onSelectCategory = category => {
    //filter restaurants
    let filteredRes = restaurantData.filter(i =>
      i.categories.includes(category.id),
    );
    setRestaurants(filteredRes);

    setSelectedCategory(category);
  };

  return (
    <SafeAreaView style={styles.container}>
      <HomeHeader currentLocation={currentLocation} />
      <MainCategories
        categories={categories}
        selectedCategory={selectedCategory}
        onSelectCategory={onSelectCategory}
      />
      <RestaurantList
        restaurants={restaurants}
        categories={categories}
        currentLocation={currentLocation}
        navigation={navigation}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.lightGray4,
  },
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.1,
    shadowRadius: 0.3,
    elevation: 1,
  },
});

export default Home;
