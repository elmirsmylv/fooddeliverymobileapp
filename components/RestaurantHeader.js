import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {COLORS, icons, SIZES} from '../constants';

const HomeHeader = ({restaurant, navigation}) => {
  return (
    <View style={{flexDirection: 'row', height: 50}}>
      <TouchableOpacity
        style={{
          width: 50,
          justifyContent: 'center',
          paddingLeft: SIZES.padding * 2,
        }}
        onPress={() => navigation.goBack()}>
        <Image
          source={icons.back}
          resizeMode="contain"
          style={{width: 25, height: 25}}
        />
      </TouchableOpacity>

      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <View
          style={{
            width: '70%',
            height: '90%',
            backgroundColor: COLORS.lightGray3,
            borderRadius: SIZES.radius,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text style={{fontSize: SIZES.body3, fontWeight: '500'}}>
            {restaurant?.name}
          </Text>
        </View>
      </View>

      <TouchableOpacity
        style={{
          width: 50,
          justifyContent: 'center',
          paddingRight: SIZES.padding * 2,
        }}>
        <Image
          source={icons.list}
          resizeMode="contain"
          style={{width: 25, height: 25}}
        />
      </TouchableOpacity>
    </View>
  );
};

export default HomeHeader;
