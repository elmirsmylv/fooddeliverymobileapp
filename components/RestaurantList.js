/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, TouchableOpacity, FlatList, Image} from 'react-native';
import {COLORS, FONTS, icons, SIZES} from '../constants';

const RestaurantList = ({
  restaurants,
  categories,
  currentLocation,
  navigation,
}) => {
  const renderItem = ({item}) => {
    const getCategoryById = id => {
      let category = categories.filter(a => a.id === id);

      if (category) {
        return category[0].name;
      } else {
        return '';
      }
    };

    return (
      <TouchableOpacity
        style={{marginBottom: SIZES.padding * 2}}
        onPress={() =>
          navigation.navigate('Restaurant', {
            item,
            currentLocation,
          })
        }>
        <View>
          <Image
            source={item.photo}
            resizeMode="cover"
            style={{width: '100%', height: 200, borderRadius: SIZES.radius}}
          />

          <View
            style={{
              position: 'absolute',
              bottom: 0,
              height: 50,
              width: SIZES.width * 0.3,
              backgroundColor: COLORS.white,
              borderBottomLeftRadius: SIZES.radius,
              borderTopRightRadius: SIZES.radius,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text style={{...FONTS.body3, fontWeight: '500'}}>
              {item.duration}
            </Text>
          </View>
        </View>

        <View style={{marginTop: 10}}>
          <Text style={{...FONTS.body3, marginBottom: 10}}>{item.name}</Text>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Image
              source={icons.star}
              resizeMode="contain"
              style={{
                height: 20,
                width: 20,
                tintColor: COLORS.primary,
                marginRight: 10,
              }}
            />
            <Text style={{...FONTS.body3}}>{item.rating}</Text>

            <View
              style={{
                flexDirection: 'row',
                marginLeft: 10,
                alignItems: 'center',
              }}>
              {item.categories.map(id => (
                <View style={{flexDirection: 'row', marginRight: 10}} key={id}>
                  <Text style={{...FONTS.body3, marginRight: 10}}>
                    {getCategoryById(id)}
                  </Text>
                  <Text style={{...FONTS.body3, color: COLORS.darkgray}}>
                    .
                  </Text>
                </View>
              ))}

              {[1, 2, 3].map(priceRat => (
                <Text
                  key={priceRat}
                  style={{
                    ...FONTS.body3,
                    color:
                      priceRat <= item.priceRating
                        ? COLORS.black
                        : COLORS.darkgray,
                  }}>
                  $
                </Text>
              ))}
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <FlatList
      data={restaurants}
      keyExtractor={item => item.id}
      renderItem={renderItem}
      contentContainerStyle={{
        paddingHorizontal: SIZES.padding * 2,
        paddingBottom: 30,
      }}
    />
  );
};

export default RestaurantList;
