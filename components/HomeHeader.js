import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {COLORS, icons, SIZES} from '../constants';

const HomeHeader = ({currentLocation}) => {
  return (
    <View style={{flexDirection: 'row', height: 50}}>
      <TouchableOpacity
        style={{
          width: 50,
          justifyContent: 'center',
          paddingLeft: SIZES.padding * 2,
        }}>
        <Image
          source={icons.nearby}
          resizeMode="contain"
          style={{width: 25, height: 25}}
        />
      </TouchableOpacity>

      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <View
          style={{
            width: '70%',
            height: '90%',
            backgroundColor: COLORS.lightGray3,
            borderRadius: SIZES.radius,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text style={{fontSize: SIZES.h4}}>
            {currentLocation?.streetName}
          </Text>
        </View>
      </View>

      <TouchableOpacity
        style={{
          width: 50,
          justifyContent: 'center',
          paddingRight: SIZES.padding * 2,
        }}>
        <Image
          source={icons.basket}
          resizeMode="contain"
          style={{width: 25, height: 25}}
        />
      </TouchableOpacity>
    </View>
  );
};

export default HomeHeader;
